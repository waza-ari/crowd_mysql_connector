# Crowd MySQL Connector

This Crowd Plugin allows users and groups to authenticate against any MySQL database. A pooled database connection
is established, which allows to query for users, groups, and group memberships.

This extension is released as Open Source, licensed under the Apache 2.0 license

Homepage: http://www.d-herrmann.de/projects/crowd-mysql-connector/

## Installation – Pre-Compiled Jar File

If you choose to use the pre-compiled JAR archive, upload it to your Crowd server and place it in %CROWDHOME%/webapp/WEB-INF/lib. Remember to set correct permissions, and then restart Crowd. Go on with the configuration section. Continue with configuration section.

## Manual Compilation

If you want to make further changes, you need to compile the Directory on your own. There are several requirements:

* Java JDK 1.6 or higher
* Apache Maven

On Windows, make sure the mvn executable is in your path. The compile the code, follow the following steps. You need to compile the sources and generate a jar file. A POM file is provided with the project, which includes the required MAVEN configuration.

* Download and extract the files to a directory
* Build them with `mvn install`
* Copy the generated JAR file to your crowd installation: %CROWDHOME%/webapp/WEB-INF/lib

## Configuration

Finally, in Crowd, you need to create a Custom Directory and perform some configuration steps.

* Create a new custom directory with class “de.d_herrmann.crowd.directory.kras.MySQLDirectoryServer”
* De-activate all directory permissions such as Create/Add/Edit User or Group
* Further, you need to configure the database parameters, this is done using the crowd directory attributes

The following attributes are available. The table lists the same, a description and a default value, which is used if the attribute is not set. Unfortunately Crowd does not seem to apply the new attribute map at runtime, so in the meantime, please restart Crowd after setting the attributes.

|Attribute              |Default    | Description                                                                                           |
|-----------------------|-----------|-------------------------------------------------------------------------------------------------------|
|db.host                        |localhost  |IP or DNS Name of the MySQL Database Server|
|db.name                        |database   |Name of the Database|
|db.username                    |username   |Username to connect to the database|
|db.password                    |           |Password of the user|
|user.tablename                 |user       |Name of the table which stores the users|
|group.tablename                |group      |Name of the table which stores the groups|
|usergroup.tablename            |usergroup  |Name of the table which maps the users to their groups. See note below for details|
|user.idfield                   |id         |User Column Name which stores the ID|
|user.namefield                 |username   |User Column Name which stores the username|
|user.firstnamefield            |firstname  |User Column Name which stores the first name|
|user.lastnamefield             |lastname   |User Column Name which stores the last name|
|user.activefield               |active     |User Column Name which stores whether the user is active or not|
|user.birthdatefield            |birthdate  |User Column Name which stores the birthdate|
|user.passwordfield             |password   |User Column Name which stores the encrypted password|
|user.emailfield                |email      |User Column Name which stores the email|
|group.namefield                |name       |Group Column Name which stores the group name|
|usergroup.userfield            |user       |Field of the mapping table which stores the user identifier|
|usergroup.groupfield           |group      |Field of the mapping table which stores the group identifier|
|hash.saltfield                 |           |Field of the user table which is used as salt value in password hash calculation|
|hash.saltmethod                |none       |Method used to salt the password. Available methods are:<br /><ul><li>none</li><li>salt_hash</li><li>hash_salt_hash</li></ul><br />For details refer to the following documentation.|
|hash.hashalgorithm             |md5        |Algorithm used to hash the password. Available algorithms are:<br /><ul><li>md5</li><li>sha1</li><li>sha256</li><li>sha512</li></ul><br />For details refer to the following documentation.|
|c3p0.minpoolsize               |3          |Minimum size of the connection pool. Refer to [c3p0 documentation](http://www.mchange.com/projects/c3p0/#minPoolSize) for further details.|
|c3p0.maxpoolsize               |10         |Maximum size of the connection pool. Refer to [c3p0 documentation](http://www.mchange.com/projects/c3p0/#maxPoolSize) for further details.|
|c3p0.acquireincrement          |2          |Number of connections which will be acquired at once. Refer to [c3p0 documentation](http://www.mchange.com/projects/c3p0/#acquireIncrement) for further details.|
|c3p0.maxidletime               |10         |Maximum number of seconds a connection can be pooled unused before it is closed. Refer to [c3p0 documentation](http://www.mchange.com/projects/c3p0/#maxIdleTime) for further details.|
|c3p0.testconnectiononcheckout  |true       |Enable connection testing on checkout. Refer to [c3p0 documentation](http://www.mchange.com/projects/c3p0/#testConnectionOnCheckout) for further details.|
|c3p0.automatictesttable        |c3p0-test  |Defines the name of the connection test table. Refer to [c3p0 documentation](http://www.mchange.com/projects/c3p0/#automaticTestTable) for further details.|

## Hashing and Salting

As described in the table, there are various methods to hash a passwords using the hash algorithms listed in the table. The salt methods however require some further explanation:

* *none refers* to plain hashing of the password.
* *salt_hash* refers to normal salting, which concatenating the hash and the salt, and hashing the resulting string
* *hash_salt_hash* refers to an advanced hashing method. First, the password is hashed. The salt is appended to the resulting hash, and this string is hashed again.

## Known Issues

* Nested Groups are not supported
* Modifying, Creating and Deleting Users and Groups is not supported
* Groups are always active, it is not possible to de-activate groups
/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.d_herrmann.crowd.directory.kras;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.model.DirectoryEntity;
import com.atlassian.crowd.model.user.User;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.Set;

/**
 * Represents a MySQL User Object
 */
public class MySQLUser implements User, DirectoryEntity, Serializable, UserWithAttributes {

    /**
     * User Attributes
     */
    private long _id;
    private String _firstname;
    private String _username;
    private String _lastname;
    private String _email;
    private Boolean _active;
    private String _credential;
    private Date _birthdate;
    private Long _directoryID;

    /**
     * Constructor of this User, it fetches the userdata from the database
     *
     * @param rs the ResultSet
     * @param directory The Directory where this user belongs to
     * @throws SQLException
     */
    public MySQLUser(ResultSet rs, RemoteDirectory directory) throws SQLException {
        //Set directoryID
        this.setDirectoryID(directory.getDirectoryId());

        //Set fieldnames
        String idfield = (directory.getValue("user.idfield") == null) ? "id" : directory.getValue("user.idfield");
        String firstnamefield = (directory.getValue("user.firstnamefield") == null) ? "firstname" : directory.getValue("user.firstnamefield");
        String lastnamefield = (directory.getValue("user.lastnamefield") == null) ? "lastname" : directory.getValue("user.lastnamefield");
        String usernamefield = (directory.getValue("user.namefield") == null) ? "username" : directory.getValue("user.namefield");
        String activefield = (directory.getValue("user.activefield") == null) ? "active" : directory.getValue("user.activefield");
        String birthdatefield = (directory.getValue("user.birthdatefield") == null) ? "birthdate" : directory.getValue("user.birthdatefield");
        String passwordfield = (directory.getValue("user.passwordfield") == null) ? "password" : directory.getValue("user.passwordfield");
        String emailfield = (directory.getValue("user.emailfield") == null) ? "email" : directory.getValue("user.emailfield");

        //Set values
        this._id = rs.getLong(idfield);
        this._firstname = rs.getString(firstnamefield);
        this._lastname = rs.getString(lastnamefield);
        this._username = rs.getString(usernamefield);
        this._active = rs.getBoolean(activefield);
        this._birthdate = rs.getDate(birthdatefield);
        this._credential = rs.getString(passwordfield);

        //Get Mails
        String mails = rs.getString(emailfield);
        if (mails == null) {
            this._email = null;
        } else {
            String[] mailsSplit = mails.split(",");
            this._email = mailsSplit[0];
        }
    }

    @Override
    public String getFirstName() {
        return this._firstname;
    }

    @Override
    public String getLastName() {
        return this._lastname;
    }

    @Override
    public String getExternalId() {
        return Long.toString(this._id);
    }

    @Override
    public long getDirectoryId() {
        return this._directoryID;
    }

    @Override
    public boolean isActive() {
        return this._active;
    }

    @Override
    public String getEmailAddress() {
        return this._email;
    }

    @Override
    public String getDisplayName() {
        return this.getFirstName()+" "+this.getLastName();
    }

    @Override
    public int compareTo(com.atlassian.crowd.embedded.api.User user) {
        return 0;
    }

    @Override
    public String getName() {
        return this._username;
    }

    /**
     * Sets the directory ID to the given value
     *
     * @param directoryID ID of this directory
     */
    public void setDirectoryID(long directoryID) {
        this._directoryID = directoryID;
    }

    public String getCredential() {
        return _credential;
    }

    public Date getBirthdate() {
        return _birthdate;
    }

    @Override
    public Set<String> getValues(String s) {
        return Collections.emptySet();
    }

    @Override
    public String getValue(String s) {
        return "";
    }

    @Override
    public Set<String> getKeys() {
        return Collections.emptySet();
    }

    @Override
    public boolean isEmpty() {
        return true;
    }
}

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.d_herrmann.crowd.directory.kras;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.GroupWithAttributes;

import javax.annotation.Nullable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Set;

/**
 * This class represents a group in the database. Nested groups currently are not supported
 */
public class MySQLGroup implements Group, GroupWithAttributes {

    /**
     * name of the group
     */
    private String _name;

    /**
     * ID of this directory in crowd
     */
    private long _directoryID;

    public MySQLGroup(ResultSet rs, RemoteDirectory directory) throws SQLException {
        this._directoryID = directory.getDirectoryId();

        String groupnamefield = (directory.getValue("group.namefield") == null) ? "name" : directory.getValue("group.namefield");

        //Set values
        this._name = rs.getString(groupnamefield);

    }

    /**
     * Constructor of this class, simply sets the name
     *
     * @param name Name of this group
     */
    /**public MySQLGroup(String name) {
        this._name = name;
    }**/

    /**
     * Returns the type of this group. We assume always Groups here, no legacy roles
     *
     * @return GroupType.GROUP
     */
    @Override
    public GroupType getType() {
        return GroupType.GROUP;
    }

    /**
     * Groups are always active
     *
     * @return true
     */
    @Override
    public boolean isActive() {
        return true;
    }

    /**
     * Gets the description of this group. As group currently do not have descriptions, simply return the name
     *
     * @return description of this group
     */
    @Override
    public String getDescription() {
        return this.getName();
    }

    @Override
    public int compareTo(Group o) {
        //TODO: implement
        return 0;
    }

    /**
     * Returns the directory ID where this group belongs to
     *
     * @return the directory ID where this group belongs to
     */
    @Override
    public long getDirectoryId() {
        return this._directoryID;
    }

    /**
     * During initialization, the directory ID is set
     *
     * @param id directory ID where these groups are used in
     */
    public void setDirectoryId(long id) {
        this._directoryID = id;
    }

    /**
     * Returns the name of this group
     *
     * @return the name of this group
     */
    @Override
    public String getName() {
        return this._name;
    }


    @Override
    public String toString() {
        return this._name;
    }

    @Nullable
    @Override
    public Set<String> getValues(String s) {
        return Collections.emptySet();
    }

    @Override
    public String getValue(String s) {
        return "";
    }

    @Override
    public Set<String> getKeys() {
        return Collections.emptySet();
    }

    @Override
    public boolean isEmpty() {
        return true;
    }
}

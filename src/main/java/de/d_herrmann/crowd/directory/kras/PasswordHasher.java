package de.d_herrmann.crowd.directory.kras;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Password hasher responsible to hash passwords based on a algorithm and possibly on a salt
 */
public class PasswordHasher {

    /**
     * Hash algorithm to hash to hash a password
     */
    private HashAlgorithm algorithm;

    /**
     * Salt method
     */
    private SaltMethod saltMethod;

    /**
     * Salt to use, might be null
     */
    private String salt = null;

    /**
     * Constructor of this class
     *
     * @param algorithm algorithm used to hash the password
     * @param saltMethod salt method to use
     * @param salt salt string
     */
    public PasswordHasher(HashAlgorithm algorithm, SaltMethod saltMethod, String salt) {
        this.algorithm = algorithm;
        this.saltMethod = saltMethod;
        this.salt = salt;
    }

    /**
     * Hashes the given password using the settings of the PasswordHasher
     *
     * @param password The password to hash
     * @return hashed password
     */
    public String getHash(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if (this.algorithm == HashAlgorithm.MD5) {
            if (this.saltMethod == SaltMethod.NONE) {
                return this.createHash(password, "MD5");
            } else if (this.saltMethod == SaltMethod.SALTHASH) {
                return this.createHash(password + this.salt, "MD5");
            } else if (this.saltMethod == SaltMethod.HASHSALTHASH) {
                return this.createHash(this.createHash(password, "MD5") + this.salt, "MD5");
            } else throw new UnsupportedEncodingException();
        } else if (this.algorithm == HashAlgorithm.SHA1) {
            if (this.saltMethod == SaltMethod.NONE) {
                return this.createHash(password, "SHA-1");
            } else if (this.saltMethod == SaltMethod.SALTHASH) {
                return this.createHash(password + this.salt, "SHA-1");
            } else if (this.saltMethod == SaltMethod.HASHSALTHASH) {
                return this.createHash(this.createHash(password, "SHA-1") + this.salt, "SHA-1");
            } else throw new UnsupportedEncodingException();
        } else if (this.algorithm == HashAlgorithm.SHA256) {
            if (this.saltMethod == SaltMethod.NONE) {
                return this.createHash(password, "SHA-256");
            } else if (this.saltMethod == SaltMethod.SALTHASH) {
                return this.createHash(password + this.salt, "SHA-256");
            } else if (this.saltMethod == SaltMethod.HASHSALTHASH) {
                return this.createHash(this.createHash(password, "SHA-256") + this.salt, "SHA-256");
            } else throw new UnsupportedEncodingException();
        } else if (this.algorithm == HashAlgorithm.SHA512) {
            if (this.saltMethod == SaltMethod.NONE) {
                return this.createHash(password, "SHA-512");
            } else if (this.saltMethod == SaltMethod.SALTHASH) {
                return this.createHash(password + this.salt, "SHA-512");
            } else if (this.saltMethod == SaltMethod.HASHSALTHASH) {
                return this.createHash(this.createHash(password, "SHA-512") + this.salt, "SHA-512");
            } else throw new UnsupportedEncodingException();
        } else throw new NoSuchAlgorithmException();
    }

    /**
     * Calculates an Hash of a given String
     *
     * @param password Initial String
     * @param hashAlgorithm String name of the hash algorithm to use, based on MessageDigest algorithm names
     * @return SHA512 hash of string
     */
    private String createHash(String password, String hashAlgorithm) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        MessageDigest md = MessageDigest.getInstance(hashAlgorithm);
        byte hash[] = md.digest(password.getBytes("UTF-8"));

        //convert the byte to hex format method 1
        StringBuilder sb = new StringBuilder();
        for (byte aHash : hash) {
            sb.append(Integer.toString((aHash & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

}

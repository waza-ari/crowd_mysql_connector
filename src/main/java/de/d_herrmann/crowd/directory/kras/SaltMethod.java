package de.d_herrmann.crowd.directory.kras;

/**
 * Enum representing different selt methods which are supported in this application
 */
public enum SaltMethod {

    //Enum
    NONE("none"),
    HASHSALTHASH("hash_salt_hash"),
    SALTHASH("salt_hash");

    //Value of this enum
    private String value;

    /**
     * Conctructor setting this enum
     *
     * @param value value of this enum
     */
    private SaltMethod(String value) {
        this.value = value;
    }

    /**
     * Returns the value of this enum
     *
     * @return value of this enum
     */
    public String getValue() {
        return value;
    }

}

package de.d_herrmann.crowd.directory.kras;

/**
 * Enum representing different hash algorithms which are supported in this application
 */
public enum HashAlgorithm {

    //Enum
    SHA1("sha1"),
    SHA256("sha256"),
    SHA512("sha512"),
    MD5("md5");

    //Value of this enum
    private String value;

    /**
     * Constructor setting this enum
     *
     * @param value value of this enum
     */
    private HashAlgorithm(String value) {
        this.value = value;
    }

    /**
     * Returns the value of this enum
     *
     * @return value of this enum
     */
    public String getValue() {
        return value;
    }

}

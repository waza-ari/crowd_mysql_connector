package de.d_herrmann.crowd.directory.kras;


import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.NullRestriction;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Helper class for the MySQL Authenticator
 * Primarily provides a method to transform Atlassian SearchRestriction to a valid SQL where clause
 */
public class MySQLHelper {

    /**
     * HashMap containing possible Property Keys and their corresponding database value
     */
    private static final Map<String, String> attributes;

    /**
     * Static constructor, initialize the attribute map
     */
    static {
        attributes = new HashMap<String, String>();
        attributes.put("ACTIVE", "active");
        attributes.put("CREATED_DATE", "mtime");
        attributes.put("EXTERNAL_ID", "id");
        attributes.put("FIRST_NAME", "prename");
        attributes.put("LAST_NAME", "name");
        attributes.put("UPDATED_DATE", "mtime");
        attributes.put("USERNAME", "username");
        attributes.put("DESCRIPTION", "name");
        attributes.put("NAME", "name");
    }

    /**
     * Transform an Atlassian SearchRestriction to a MySQL where clause
     *
     * @param sr SearchRestriction which needs to be transformed
     * @return SQL where clause matching the given search restriction
     */
    public static String transformSearchRestriction(SearchRestriction sr) {
        //If boolean, there might be multiple restrictions collected by an boolean operator
        if (sr instanceof BooleanRestriction) {
            String operator = ((BooleanRestriction) sr).getBooleanLogic().name();
            ArrayList<SearchRestriction> srs = new ArrayList<SearchRestriction>(((BooleanRestriction) sr).getRestrictions());

            StringBuilder filterResult = new StringBuilder();
            String separator = "";
            for (SearchRestriction s : srs) {
                //Do not append anything in the first run
                filterResult.append(separator);

                //Evaluate this filter
                filterResult.append("(");
                filterResult.append(MySQLHelper.transformSearchRestriction(s));
                filterResult.append(")");

                //From the second iteration onwards, use boolean operator to connect
                separator = " "+operator+" ";
            }

            return filterResult.toString();
        } else if (sr instanceof NullRestriction) {
            //NullRestriction indicated that there are no restrictions - answer empty filter
            return "";
        } else if (sr instanceof PropertyRestriction) {

            //Property Restriction is responsible for actually comparing values

            String matchmode = ((PropertyRestriction) sr).getMatchMode().name();
            String propName = ((PropertyRestriction) sr).getProperty().getPropertyName();
            String value;

            //Extract the value, which might either be an object, or a String
            if (((PropertyRestriction) sr).getProperty().getPropertyType() == String.class) {
                value = (String) ((PropertyRestriction) sr).getValue();
            } else {
                value = ((PropertyRestriction) sr).getValue().toString();
            }

            //Get correct database field name of property name
            // Find user property names here: https://docs.atlassian.com/crowd/latest/com/atlassian/crowd/search/query/entity/restriction/constants/UserTermKeys.html
            // Find group property names here: https://docs.atlassian.com/crowd/latest/com/atlassian/crowd/search/query/entity/restriction/constants/GroupTermKeys.html

            //Handle DISPLAY_NAME separately, as we need to search both first name and last name
            String matchField;
            if (propName.equals(" DISPLAY_NAME")) {
                matchField = "CONCAT(" + attributes.get("FIRST_NAME") + ", ' ', " + attributes.get("LAST_NAME") + ")";
            } else {
                matchField = attributes.get(propName);
            }

            //Calculate operator - matchmode might be CONTAINS, EXACTLY_MATCHES, GREATER_THAN, LESS_THAN, NULL, STARTS_WITH
            if (matchmode.equals("CONTAINS")) {
                return matchField + " LIKE \"%" + value + "%\"";
            } else if (matchmode.equals("EXACTLY_MATCHES")) {
                return matchField + " = \"" + value + "\"";
            } else if (matchmode.equals("GREATER_THAN")) {
                return matchField + " > \"" + value + "\"";
            } else if (matchmode.equals("LESS_THAN")) {
                return matchField + " < \"" + value + "\"";
            } else if (matchmode.equals("NULL")) {
                return matchField + " IS NULL";
            } else if (matchmode.equals("STARTS_WITH")) {
                return matchField + " LIKE \"" + value + "%\"";
            } else return "";
        } else {
            //This is an unknown condition... Log warning and return empty condition
            return "";
        }
    }
}




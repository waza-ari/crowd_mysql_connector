/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package de.d_herrmann.crowd.directory.kras;

import com.atlassian.crowd.directory.DirectoryMembershipsIterable;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.model.DirectoryEntity;
import com.atlassian.crowd.model.group.*;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.search.Entity;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.search.util.SearchResultsUtil;
import com.atlassian.crowd.util.BoundedCount;
import com.google.common.collect.ImmutableList;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyVetoException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Custom implementation of a Atlassian Crowd RemoteDirectory, which allows to authenticate
 * Principals against an existing MySQL Database
 */
public class MySQLDirectoryServer implements RemoteDirectory {

    ////////////// CONFIGURATION ///////////////////
    //Database connection - Default Configuration
    //Can be changed by setting attributes in the crowd frontend
    private static String HOSTNAME = "localhost";
    private static String DATABASE = "database";
    private static String USERNAME = "username";
    private static String PASSWORD = "";

    //Table configuration defaults
    private static String USERTABLE = "user";
    private static String GROUPTABLE = "group";
    private static String USERGROUPTABLE = "usergroup";
    
    //Field configuration defaults
    private static String USERTABLENAMEFIELD = "username";
    private static String GROUPTABLENAMEFIELD = "name";

    //Group-User Mapping Field configuration defaults
    private static String USERGROUPTABLEUSERFIELD = "user";
    private static String USERGROUPTABLEGROUPFIELD = "group";
    ////////////// END OF CONFIGURATION ///////////////////

    /**
     * ID of this directory
     */
    private volatile long _directoryId = 12345;

    /**
     * Attributes of this directory. Those are immutable and are set by the CROWD framework
     */
    private Map<String, String> _directoryAttributes;

    /**
     * C3P0 Connection Instance, holding a pooled MySQL connection
     */
    private ComboPooledDataSource cpds;

    /**
     * Logger instance
     */
    private static final Logger log = LoggerFactory.getLogger(MySQLDirectoryServer.class);

    /**
     * Hash Methods
     */
    private HashAlgorithm hashAlgorithm;
    private SaltMethod saltMethod;
    private String saltField;

    /**
     * C3P0 settings
     */
    private int minPoolSize = 3;
    private int maxPoolSize = 10;
    private int acquireIncrement = 2;
    private boolean testConnectionOnCheckout = true;
    private int maxIdleTime = 30;
    private String testTableName = "c3p0-test";

    //////////////////// Attribute functions //////////////////////

    /**
     * Stores the attribute Map in this Directory implementation. This function is called by the CROWD Framework 
     * 
     * @param attributes A Map containing attributes, which can be set at the Crowd frontend by the user
     */
    @Override
    public void setAttributes(Map<String, String> attributes) {
        this._directoryAttributes = attributes;
        
        //Database Connection Settings
        if (this.getValue("db.host") != null) HOSTNAME = this.getValue("db.host");
        if (this.getValue("db.name") != null) DATABASE = this.getValue("db.name");
        if (this.getValue("db.username") != null) USERNAME = this.getValue("db.username");
        if (this.getValue("db.password") != null) PASSWORD = this.getValue("db.password");

        //Table settings
        if (this.getValue("user.tablename") != null) USERTABLE = this.getValue("user.tablename");
        if (this.getValue("group.tablename") != null) GROUPTABLE = this.getValue("group.tablename");
        if (this.getValue("usergroup.tablename") != null) USERGROUPTABLE = this.getValue("usergroup.tablename");

        //Field settings
        if (this.getValue("user.namefield") != null) USERTABLENAMEFIELD = this.getValue("user.namefield");
        if (this.getValue("group.namefield") != null) GROUPTABLENAMEFIELD = this.getValue("group.namefield");
        if (this.getValue("usergroup.userfield") != null) USERGROUPTABLEUSERFIELD = this.getValue("usergroup.userfield");
        if (this.getValue("usergroup.groupfield") != null) USERGROUPTABLEGROUPFIELD = this.getValue("usergroup.groupfield");

        //Hash Settings
        if (this.getValue("hash.saltfield") != null) this.saltField = this.getValue("hash.saltfield");

        //Salt Method defaults to none
        if (this.getValue("hash.saltmethod").equals(SaltMethod.SALTHASH.getValue())) {
            this.saltMethod = SaltMethod.SALTHASH;
        } else if (this.getValue("hash.saltmethod").equals(SaltMethod.HASHSALTHASH.getValue())) {
            this.saltMethod = SaltMethod.HASHSALTHASH;
        } else {
            this.saltMethod = SaltMethod.NONE;
        }
        //Hash Method defaults to MD5
        if (this.getValue("hash.hashalgorithm").equals(HashAlgorithm.SHA1.getValue())) {
            this.hashAlgorithm = HashAlgorithm.SHA1;
        } else if (this.getValue("hash.hashalgorithm").equals(HashAlgorithm.SHA256.getValue())) {
            this.hashAlgorithm = HashAlgorithm.SHA256;
        } else if (this.getValue("hash.hashalgorithm").equals(HashAlgorithm.SHA512.getValue())) {
            this.hashAlgorithm = HashAlgorithm.SHA512;
        } else {
            this.hashAlgorithm = HashAlgorithm.MD5;
        }

        //C3P0 Settings
        if (this.getValue("c3p0.minpoolsize") != null) this.minPoolSize = Integer.valueOf(this.getValue("c3p0.minpoolsize"));
        if (this.getValue("c3p0.maxpoolsize") != null) this.maxPoolSize = Integer.valueOf(this.getValue("c3p0.maxpoolsize"));
        if (this.getValue("c3p0.acquireincrement") != null) this.acquireIncrement = Integer.valueOf(this.getValue("c3p0.acquireincrement"));
        if (this.getValue("c3p0.maxidletime") != null) this.maxIdleTime = Integer.valueOf(this.getValue("c3p0.maxidletime"));
        if (this.getValue("c3p0.testconnectiononcheckout") != null) this.testConnectionOnCheckout = this.getValue("c3p0.maxpoolsize").equals("true");
        if (this.getValue("c3p0.automatictesttable") != null) this.testTableName = this.getValue("c3p0.automatictesttable");
        
        //Now, we can create database connection
        this.createSQLConnection();
    }

    //////////////////// Helper functions /////////////////////////

    /**
     * Sets up the MySQL Connection Object
     */
    private void createSQLConnection() {
        //Create database connection using c3p0
         cpds = new ComboPooledDataSource();
        try {
            cpds.setDriverClass("com.mysql.jdbc.Driver");
            cpds.setJdbcUrl("jdbc:mysql://"+HOSTNAME+":3306/"+DATABASE+"?zeroDateTimeBehavior=convertToNull&characterEncoding=UTF-8&characterSetResults=UTF-8");
            cpds.setUser(USERNAME);
            cpds.setPassword(PASSWORD);

            cpds.setMinPoolSize(this.minPoolSize);
            cpds.setAcquireIncrement(this.acquireIncrement);
            cpds.setMaxPoolSize(this.maxPoolSize);
            cpds.setTestConnectionOnCheckout(this.testConnectionOnCheckout);
            cpds.setMaxIdleTime(this.maxIdleTime);
            cpds.setAutomaticTestTable(this.testTableName);
        } catch (PropertyVetoException e) {
            log.error("Failed to initialize the Database Connection to database {} at host {} - Please check connection settings: "+ e.getMessage(), DATABASE, HOSTNAME);
            log.debug(ExceptionUtils.getStackTrace(e));
        }
    }

    //////////////////// General directory functions ////////////////////
    @Override
    public long getDirectoryId() {
        return this._directoryId;
    }

    @Override
    public void setDirectoryId(final long directoryId) {
        log.debug("Setting Directory ID to " + directoryId);
        this._directoryId = directoryId;
    }

    @Override
    public String getDescriptiveName() {
        return "MySQL based authenticator for KRAS";
    }

    //////////////////// User functions ////////////////////

    @Override
    public MySQLUser findUserByName(String name) throws UserNotFoundException, OperationFailedException {
        Validate.notNull(name, "Username cannot be empty");

        ResultSet rs = null;
        Connection con = null;
        Statement findUserStmt = null;
        
        try {
            con = this.cpds.getConnection();
            findUserStmt = con.createStatement();
            String query = "SELECT * FROM `"+USERTABLE+"` WHERE `"+ USERTABLENAMEFIELD +"` = \""+name+"\";";
            rs = findUserStmt.executeQuery(query);
            
            //Get user
            if (!rs.next()) {
                throw new UserNotFoundException("Directory "+this.getDirectoryId()+" failed to find a user with username \""+name+"\"");
            } else {
                return new MySQLUser(rs, this);
            }
        } catch (SQLException e) {
            log.error("Failed to find user with exception: "+e.getMessage());
            log.debug(ExceptionUtils.getStackTrace(e));
            throw new OperationFailedException();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (findUserStmt != null)
                    findUserStmt.close();
                if (con != null && !con.isClosed())
                    con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public UserWithAttributes findUserWithAttributesByName(String s) throws UserNotFoundException, OperationFailedException {
        return this.findUserByName(s);
    }


    @Override
    public User findUserByExternalId(String s) throws UserNotFoundException, OperationFailedException {
        Validate.notNull(s, "ID cannot be empty");
        Integer id = Integer.valueOf(s);

        ResultSet rs = null;
        Connection con = null;
        Statement findUserStmt = null;

        try {
            con = this.cpds.getConnection();
            findUserStmt = con.createStatement();
            String query = "SELECT * FROM `"+USERTABLE+"` WHERE `id` = \""+id+"\";";
            rs = findUserStmt.executeQuery(query);

            //Get user
            if (!rs.next()) {
                throw new UserNotFoundException("Directory "+this.getDirectoryId()+" failed to find a user with id \""+id+"\"");
            } else {
                return new MySQLUser(rs, this);
            }
        } catch (SQLException e) {
            log.error("Failed to find user with exception: "+e.getMessage());
            log.debug(ExceptionUtils.getStackTrace(e));
            throw new OperationFailedException();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (findUserStmt != null)
                    findUserStmt.close();
                if (con != null && !con.isClosed())
                    con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public User authenticate(String name, PasswordCredential credential) throws UserNotFoundException, InactiveAccountException, InvalidAuthenticationException, ExpiredCredentialException, OperationFailedException {

        log.info("MySQL Auth: Authenticating user "+name);

        MySQLUser user = this.findUserByName(name);

        if (!user.isActive())
            throw new InactiveAccountException("This account is not active.");

        //Get Password from database
        String encryptedPassword = user.getCredential();
        log.debug("Stored password: " + encryptedPassword);

        //Hash entered password based on attribute settings
        //Get salt if salt method is defined
        String salt = null;
        if (this.saltMethod != SaltMethod.NONE) {
            //DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
            //salt =  df.format(user.getBirthdate());
            
            Connection con = null;
            ResultSet rs = null;
            Statement findSaltStmt = null;
            
            try {
                con = this.cpds.getConnection();
                findSaltStmt = con.createStatement();
                String query = "SELECT `"+this.saltField+"` FROM `"+USERTABLE+"` WHERE `"+ USERTABLENAMEFIELD +"` = \""+user.getName()+"\";";
                rs = findSaltStmt.executeQuery(query);

                //Get user
                if (!rs.next()) {
                    log.warn("Unable to calculate password hash: Failed to retrieve salt from table. User was not found");
                } else {
                    salt = rs.getString(this.saltField);
                }
            } catch (SQLException e) {
                log.warn("Unable to calculate password hash: Failed to retrieve salt from table: "+e.getMessage());
                log.debug(ExceptionUtils.getStackTrace(e));
            }  finally {
                try {
                    if (rs != null)
                        rs.close();
                    if (findSaltStmt != null)
                        findSaltStmt.close();
                    if (con != null && !con.isClosed())
                        con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        PasswordHasher ph = new PasswordHasher(this.hashAlgorithm, this.saltMethod, salt);

        String enteredPassword = null;
        try {
            enteredPassword = ph.getHash(credential.getCredential());
        } catch (NoSuchAlgorithmException e) {
            log.warn("Unable to calculate password hash of user: "+e.getMessage());
            log.debug(ExceptionUtils.getStackTrace(e));
        } catch (UnsupportedEncodingException e) {
            log.warn("Unable to calculate password hash of user: "+e.getMessage());
            log.debug(ExceptionUtils.getStackTrace(e));
        }

        log.debug("Entered password: " + enteredPassword);

        if (encryptedPassword.equals(enteredPassword)) {
            return user;
        } else {
            log.warn("Unable to authenticate user {} - passwords do not match.", name);
            throw new InvalidAuthenticationException("Invalid user credentials");
        }
    }

    @Override
    public User addUser(UserTemplate user, PasswordCredential credential) throws InvalidUserException, InvalidCredentialException, UserAlreadyExistsException, OperationFailedException {
        //TODO: implement
        return null;
    }

    @Override
    public User updateUser(UserTemplate user) throws InvalidUserException, UserNotFoundException, OperationFailedException {
        //TODO: implement
        return null;
    }

    @Override
    public void updateUserCredential(String username, PasswordCredential credential) throws UserNotFoundException, InvalidCredentialException, OperationFailedException {
        //TODO: implement

    }

    @Override
    public User renameUser(String oldName, String newName) throws UserNotFoundException, InvalidUserException, UserAlreadyExistsException, OperationFailedException {
        //TODO: implement
        return null;
    }

    @Override
    public void storeUserAttributes(String username, Map<String, Set<String>> attributes) throws UserNotFoundException, OperationFailedException {
        //TODO: implement

    }

    @Override
    public void removeUserAttributes(String username, String attributeName) throws UserNotFoundException, OperationFailedException {
        //TODO: implement

    }

    @Override
    public void removeUser(String name) throws UserNotFoundException, OperationFailedException {
        //TODO: implement

    }

    @Override
    public <T> List<T> searchUsers(EntityQuery<T> query) throws OperationFailedException {
        Validate.notNull(query, "query cannot be null");

        //Correct SQL query
        Integer maxResults = query.getMaxResults();
        if (maxResults == -1) maxResults = 1844674407;

        String filter = MySQLHelper.transformSearchRestriction(query.getSearchRestriction());
        if (!filter.isEmpty()) {
            filter = " WHERE "+filter;
        }

        //Fetch the users
        ResultSet rs = null;
        Statement findUserStmt = null;
        Connection con = null;
        
        ArrayList<MySQLUser> users = new ArrayList<MySQLUser>();
        try {
            con = this.cpds.getConnection();
            findUserStmt = con.createStatement();
            String sqlQuery = "SELECT * FROM `"+USERTABLE+"`"+filter+";";
            rs = findUserStmt.executeQuery(sqlQuery);

            //Get users
            while (rs.next()) {
                users.add(new MySQLUser(rs, this));
            }
        } catch (SQLException e) {
            log.error("Failed to find user with exception: "+e.getMessage());
            log.debug(ExceptionUtils.getStackTrace(e));
            throw new OperationFailedException();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (findUserStmt != null)
                    findUserStmt.close();
                if (con != null && !con.isClosed())
                    con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        //Check whether we are asked to return as name or as object
        if (query.getReturnType() == String.class) {
            ArrayList<String> userNames = new ArrayList<String>();
            for (MySQLUser u : users) {
                userNames.add(u.getName());
            }
            return (List<T>) userNames;
        } else {
            return (List<T>)users;
        }
    }

    @Override
    public MySQLGroup findGroupByName(String name) throws GroupNotFoundException, OperationFailedException {
        Validate.notNull(name, "Groupname cannot be empty");

        ResultSet rs = null;
        Connection con = null;
        Statement findGroupStmt = null;
        try {
            con = this.cpds.getConnection();
            findGroupStmt = con.createStatement();
            String query = "SELECT * FROM `"+GROUPTABLE+"` WHERE `"+GROUPTABLENAMEFIELD+"` = \""+name+"\";";
            rs = findGroupStmt.executeQuery(query);

            //Get user
            if (!rs.next()) {
                throw new GroupNotFoundException("Directory "+this.getDirectoryId()+" failed to find a group with name \""+name+"\"");
            } else {
                return new MySQLGroup(rs, this);
            }
        } catch (SQLException e) {
            log.error("Failed to find group with exception: "+e.getMessage());
            log.debug(ExceptionUtils.getStackTrace(e));
            throw new OperationFailedException();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (findGroupStmt != null)
                    findGroupStmt.close();
                if (con != null && !con.isClosed())
                    con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public GroupWithAttributes findGroupWithAttributesByName(String name) throws GroupNotFoundException, OperationFailedException {
        return this.findGroupByName(name);
    }

    @Override
    public Group addGroup(GroupTemplate group) throws InvalidGroupException, OperationFailedException {
        //TODO: implement
        return null;
    }

    @Override
    public Group updateGroup(GroupTemplate group) throws InvalidGroupException, GroupNotFoundException, ReadOnlyGroupException, OperationFailedException {
        //TODO: implement
        return null;
    }

    @Override
    public Group renameGroup(String oldName, String newName) throws GroupNotFoundException, InvalidGroupException, OperationFailedException {
        //TODO: implement
        return null;
    }

    @Override
    public void storeGroupAttributes(String groupName, Map<String, Set<String>> attributes) throws GroupNotFoundException, OperationFailedException {
        //TODO: implement

    }

    @Override
    public void removeGroupAttributes(String groupName, String attributeName) throws GroupNotFoundException, OperationFailedException {
        //TODO: implement

    }

    @Override
    public void removeGroup(String name) throws GroupNotFoundException, ReadOnlyGroupException, OperationFailedException {
        //TODO: implement

    }

    @Override
    public <T> List<T> searchGroups(EntityQuery<T> query) throws OperationFailedException {
        //Correct SQL query
        Integer maxResults = query.getMaxResults();
        if (maxResults == -1) maxResults = 1844674407;

        String filter = MySQLHelper.transformSearchRestriction(query.getSearchRestriction());
        if (!filter.isEmpty()) {
            filter = " WHERE "+filter;
        }

        //Fetch the users
        ResultSet rs = null;
        Statement findGroupStmt = null;
        Connection con = null;
        
        ArrayList<MySQLGroup> groups = new ArrayList<MySQLGroup>();
        try {
            con = this.cpds.getConnection();
            findGroupStmt = con.createStatement();
            String sqlQuery = "SELECT * FROM `"+GROUPTABLE+"`"+filter+";";
            rs = findGroupStmt.executeQuery(sqlQuery);

            //Get groups
            while (rs.next()) {
                groups.add(new MySQLGroup(rs, this));
            }
        } catch (SQLException e) {
            log.error("Failed to find group with exception: "+e.getMessage());
            log.debug(ExceptionUtils.getStackTrace(e));
            throw new OperationFailedException(filter);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (findGroupStmt != null)
                    findGroupStmt.close();
                if (con != null && !con.isClosed())
                    con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        //Check whether we are asked to return as name or as object
        if (query.getReturnType() == String.class) {
            ArrayList<String> groupNames = new ArrayList<String>();
            for (MySQLGroup g : groups) {
                groupNames.add(g.getName());
            }
            return (List<T>) groupNames;
        } else {
            return (List<T>)groups;
        }
    }

    @Override
    public boolean isUserDirectGroupMember(String username, String groupName) throws OperationFailedException {
        
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement prep = null;
        
        try {
            con = this.cpds.getConnection();
            prep = con.prepareStatement("SELECT COUNT(`id`) AS `isMember` FROM `" + USERGROUPTABLE + "` WHERE `" + USERGROUPTABLEGROUPFIELD + "` LIKE ? AND `" + USERGROUPTABLEUSERFIELD + "` = ?");

            User user = this.findUserByName(username);
            prep.setString(1, groupName);
            prep.setString(2, user.getExternalId());

            rs = prep.executeQuery();
            rs.next();

            return rs.getInt("isMember") > 0;

        } catch (SQLException e) {
            log.debug(ExceptionUtils.getStackTrace(e));
        } catch (UserNotFoundException e) {
            log.debug(ExceptionUtils.getStackTrace(e));
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (prep != null)
                    prep.close();
                if (con != null && !con.isClosed())
                    con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    @Override
    public boolean isGroupDirectGroupMember(String childGroup, String parentGroup) throws OperationFailedException {
        //We do not support group nesting
        return false;
    }

    @Override
    public BoundedCount countDirectMembersOfGroup(String s, int i) throws OperationFailedException {
        return null;
    }

    @Override
    public void addUserToGroup(String username, String groupName) throws GroupNotFoundException, UserNotFoundException, ReadOnlyGroupException, OperationFailedException {
        //TODO: implement

    }

    @Override
    public void addGroupToGroup(String childGroup, String parentGroup) throws GroupNotFoundException, InvalidMembershipException, ReadOnlyGroupException, OperationFailedException {
        //TODO: implement

    }

    @Override
    public void removeUserFromGroup(String username, String groupName) throws GroupNotFoundException, UserNotFoundException, MembershipNotFoundException, ReadOnlyGroupException, OperationFailedException {
        //TODO: implement

    }

    @Override
    public void removeGroupFromGroup(String childGroup, String parentGroup) throws GroupNotFoundException, InvalidMembershipException, MembershipNotFoundException, ReadOnlyGroupException, OperationFailedException {
        //TODO: implement

    }

    @Override
    public <T> List<T> searchGroupRelationships(MembershipQuery<T> query) throws OperationFailedException {
        Validate.notNull(query, "query argument cannot be null");

        if (query.getEntityToMatch().getEntityType() == Entity.GROUP && query.getEntityToReturn().getEntityType() == Entity.GROUP && query.getEntityToMatch().getEntityType() != query.getEntityToReturn().getEntityType()) {
            throw new IllegalArgumentException("Cannot search for group relationships of mismatching GroupTypes: attempted to match <" + query.getEntityToMatch().getEntityType() + "> and return <" + query.getEntityToReturn().getEntityType() + ">");
        }

        Iterable<T> results;

        if (query.getEntityToMatch().getEntityType() == Entity.GROUP && query.getEntityToReturn().getEntityType() == Entity.USER) {
            GroupType groupType = query.getEntityToMatch().getGroupType();

            if (groupType == null) {
                // if groupType is null then we are searching for either the group or the role (try group first, then role)
                MembershipQuery<T> groupQuery = QueryBuilder.createMembershipQuery(query.getMaxResults(), query.getStartIndex(), query.isFindChildren(), query.getEntityToReturn(), query.getReturnType(), query.getEntityToMatch(), query.getEntityNameToMatch());
                results = searchGroupRelationshipsWithGroupTypeSpecified(groupQuery);
            } else {
                // groupType has been specified, so safe to execute directly
                results = searchGroupRelationshipsWithGroupTypeSpecified(query);
            }
        } else if (query.getEntityToMatch().getEntityType() == Entity.USER && query.getEntityToReturn().getEntityType() == Entity.GROUP) {
            GroupType groupType = query.getEntityToReturn().getGroupType();

            if (groupType == null) {
                // if groupType is null then we are searching for either the group or the role (try group first, then role)
                MembershipQuery<T> groupQuery = QueryBuilder.createMembershipQuery(query.getMaxResults(), query.getStartIndex(), query.isFindChildren(), EntityDescriptor.group(GroupType.GROUP), query.getReturnType(), query.getEntityToMatch(), query.getEntityNameToMatch());
                results = searchGroupRelationshipsWithGroupTypeSpecified(groupQuery);
            } else {
                // groupType has been specified, so safe to execute directly
                results = searchGroupRelationshipsWithGroupTypeSpecified(query);
            }
        } else if (query.getEntityToMatch().getEntityType() == Entity.GROUP && query.getEntityToReturn().getEntityType() == Entity.GROUP) {
            //Group Nesting not allowed
            return Collections.emptyList();
        } else {
            throw new IllegalArgumentException("Cannot search for relationships between a USER and another USER");
        }

        return ImmutableList.copyOf(results);
    }

    /**
     *  Converts an Iterable to a generic Iterable
     *
     * @param list typed iterable
     * @param <T> type of iterable
     * @return generic iterable
     */
    @SuppressWarnings("unchecked")
    private static <T> Iterable<T> toGenericIterable(Iterable list) {
        return (Iterable<T>) list;
    }

    /**
     * Finds User Members of the given group and returns an iterable collection of MySQLUsers
     *
     * @param groupName name of the group to search users in
     * @param groupType grouptype - not implemented in this operation
     * @param startIndex first user which sould be displayed
     * @param maxMatch maximim amount of users which should be returned
     * @return Collection of Users matching the crierias above.
     */
    private Iterable<MySQLUser> findUserMembersOfGroup(String groupName, GroupType groupType, Integer startIndex, Integer maxMatch) {
        //Correct SQL query
        Integer maxResults = maxMatch;
        if (maxResults == -1) maxResults = 1844674407;

        String sql = "SELECT `u`.* " +
                "FROM `"+USERGROUPTABLE+"` AS `dbr` " +
                "INNER JOIN `"+USERTABLE+"` AS `u` " +
                "ON `dbr`.`"+USERGROUPTABLEUSERFIELD+"` = `u`.`id` " +
                "WHERE `dbr`.`"+USERGROUPTABLEGROUPFIELD+"` = ? " +
                "GROUP BY `u`.`id` " +
                "LIMIT "+startIndex+", "+maxResults+";";

        ArrayList<MySQLUser> users = new ArrayList<MySQLUser>() {};
        
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;
        
        try {
            con = this.cpds.getConnection();
            stmt = con.prepareStatement(sql);
            stmt.setString(1, groupName);
            rs = stmt.executeQuery();
            while(rs.next()) {
                users.add(new MySQLUser(rs, this));
            }
        } catch (SQLException e) {
            log.debug(ExceptionUtils.getStackTrace(e));
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (con != null && !con.isClosed())
                    con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return users;
    }

    /**
     * Searches for group members (both users or other groups) and returns the Name of those objects as an
     * iterable collection of Strings
     *
     * @param query The Membership Query in Atlassian Format
     * @return Iterable Object of Strings matching the found objects
     * @throws OperationFailedException
     */
    protected Iterable<String> findGroupMembershipNames(final MembershipQuery<String> query) throws OperationFailedException {
        if (query.getEntityToMatch().getEntityType() == Entity.USER) {

            Connection con = null;
            ResultSet rs = null;
            PreparedStatement stmt = null;
            ArrayList<String> result = new ArrayList<String>();

            // query is to find GROUP memberships of USER
            String sql = "SELECT DISTINCT(`dbr`.`"+USERGROUPTABLEGROUPFIELD+"`) AS `name` " +
                    "FROM `"+USERGROUPTABLE+"` AS `dbr` " +
                    "INNER JOIN `"+USERTABLE+"` AS `u` " +
                    "ON `dbr`.`"+USERGROUPTABLEUSERFIELD+"` = `u`.`id` " +
                    "WHERE `u`.`"+ USERTABLENAMEFIELD +"` = ?";

            try {
                con = this.cpds.getConnection();
                stmt = con.prepareStatement(sql);
                stmt.setString(1, query.getEntityNameToMatch());
                rs = stmt.executeQuery();

                while (rs.next()) {
                    result.add(rs.getString("name"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null)
                        rs.close();
                    if (stmt != null)
                        stmt.close();
                    if (con != null && !con.isClosed())
                        con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            return result;

        } else if (query.getEntityToMatch().getEntityType() == Entity.GROUP) {
            // query is to find GROUP memberships of GROUP (only if nesting is enabled [which is not ;=)] )
            return Collections.emptyList();
        } else {
            throw new IllegalArgumentException("You can only find the GROUP memberships of USER or GROUP");
        }
    }

    /**
     * Finds the Groups to which a given user belongs to
     *
     * @param query The Membership Query in Atlassian Format
     * @return iterable collection of Groups
     * @throws OperationFailedException
     */
    protected Iterable<MySQLGroup> findGroupMemberships(final MembershipQuery<MySQLGroup> query) throws OperationFailedException {
        if (query.getEntityToMatch().getEntityType() == Entity.USER) {

            ResultSet rs = null;
            Connection con = null;
            PreparedStatement stmt = null;
            ArrayList<MySQLGroup> result = new ArrayList<MySQLGroup>();

            // query is to find GROUP memberships of USER
            String sql = "SELECT DISTINCT(`dbr`.`"+USERGROUPTABLEGROUPFIELD+"`) AS `name` " +
                    "FROM `"+USERGROUPTABLE+"` AS `dbr` " +
                    "INNER JOIN `"+USERTABLE+"` AS `u` " +
                    "ON `dbr`.`"+USERGROUPTABLEUSERFIELD+"` = `u`.`id` " +
                    "WHERE `u`.`"+ USERTABLENAMEFIELD +"` = ?";

            try {
                con = this.cpds.getConnection();
                stmt = con.prepareStatement(sql);
                stmt.setString(1, query.getEntityNameToMatch());
                rs = stmt.executeQuery();

                while (rs.next()) {
                    result.add(new MySQLGroup(rs, this));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null)
                        rs.close();
                    if (stmt != null)
                        stmt.close();
                    if (con != null && !con.isClosed())
                        con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            return result;
        } else if (query.getEntityToMatch().getEntityType() == Entity.GROUP) {
            // query is to find GROUP memberships of GROUP (only if nesting is enabled [which is not ;=) )
            return Collections.emptyList();
        } else {
            throw new IllegalArgumentException("You can only find the GROUP memberships of USER or GROUP");
        }
    }

    /**
     * Resolves a relationship query for both users and groups
     *
     * @param query relationship query
     * @param <T> type of requested object
     * @return Iterable collection of <T> objects
     * @throws OperationFailedException
     */
    protected <T> Iterable<T> searchGroupRelationshipsWithGroupTypeSpecified(final MembershipQuery<T> query)
            throws OperationFailedException {
        Iterable<? extends DirectoryEntity> relations;

        if (query.isFindChildren()) {
            if (query.getEntityToMatch().getEntityType() == Entity.GROUP) {
                if (query.getEntityToReturn().getEntityType() == Entity.USER) {
                    // query is to find USER members of GROUP
                    relations = findUserMembersOfGroup(query.getEntityNameToMatch(), query.getEntityToMatch().getGroupType(), query.getStartIndex(), query.getMaxResults());
                }
                else if (query.getEntityToReturn().getEntityType() == Entity.GROUP) {
                    // query is to find GROUP members of GROUP (nesting disabled)
                    relations = Collections.emptyList();
                } else {
                    throw new IllegalArgumentException("You can only find the GROUP or USER members of a GROUP");
                }
            } else {
                throw new IllegalArgumentException("You can only find the GROUP or USER members of a GROUP");
            }
        } else {
            // find memberships
            if (query.getEntityToReturn().getEntityType() == Entity.GROUP) {
                if (query.getEntityToReturn().getGroupType() == GroupType.GROUP) {
                    if (query.getReturnType() == String.class) {// as name
                        return toGenericIterable(findGroupMembershipNames((MembershipQuery<String>) query));
                    } else {
                        relations = findGroupMemberships((MembershipQuery<MySQLGroup>) query);
                    }
                } else if (query.getEntityToReturn().getGroupType() == GroupType.LEGACY_ROLE) {
                    return Collections.emptyList();
                } else {
                    throw new IllegalArgumentException("Cannot find group memberships of entity via member for GroupType: " + query.getEntityToReturn().getGroupType());
                }
            } else {
                throw new IllegalArgumentException("You can only find the GROUP memberships of USER or GROUP");
            }
        }

        if (query.getReturnType() == String.class) { // as name {
            return toGenericIterable(SearchResultsUtil.convertEntitiesToNames(relations));
        } else {
            return toGenericIterable(relations);
        }

    }

    @Override
    public void testConnection() throws OperationFailedException {
    }

    @Override
    public boolean supportsInactiveAccounts() {
        return true;
    }

    @Override
    public boolean supportsNestedGroups() {
        return false;
    }

    @Override
    public boolean supportsPasswordExpiration() {
        return false;
    }

    @Override
    public boolean supportsSettingEncryptedCredential() {
        return false;
    }

    @Override
    public boolean isRolesDisabled() {
        return true;
    }

    @Override
    public Iterable<Membership> getMemberships() throws OperationFailedException {
        return new DirectoryMembershipsIterable(this);
    }

    @Override
    public RemoteDirectory getAuthoritativeDirectory() {
        return this;
    }

    @Override
    public void expireAllPasswords() throws OperationFailedException {
    }

    @Override
    public Set<String> getValues(String key) {
        if (!this._directoryAttributes.keySet().contains(key)) return null;
        //TODO: implement
        return null;
    }

    @Override
    public String getValue(String key) {
        if (!this._directoryAttributes.keySet().contains(key)) return null;
        return this._directoryAttributes.get(key);
    }

    @Override
    public Set<String> getKeys() {
        return this._directoryAttributes.keySet();
    }

    @Override
    public boolean isEmpty() {
        return this._directoryAttributes.isEmpty();
    }
}
